﻿using System;

namespace StudioKit.ExternalProvider.Models.Interfaces;

public interface IScore
{
	decimal? TotalPoints { get; set; }

	DateTime DateLastUpdated { get; set; }

	string UserId { get; }
}