﻿namespace StudioKit.ExternalProvider.Models.Interfaces;

public interface IExternalGradable
{
	int Id { get; set; }

	int ExternalGroupId { get; set; }

	int GradableId { get; set; }

	string ExternalId { get; set; }
}