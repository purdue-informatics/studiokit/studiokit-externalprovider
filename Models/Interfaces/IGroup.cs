﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Models.Interfaces;

public interface IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>
	where TExternalTerm : IExternalTerm
	where TGroupUserRole : IGroupUserRole
	where TExternalGroup : IExternalGroup
{
	int Id { get; set; }

	bool IsDeleted { get; set; }

	DateTime? StartDate { get; set; }

	DateTime? EndDate { get; set; }

	int? ExternalTermId { get; set; }

	TExternalTerm ExternalTerm { get; set; }

	ICollection<TGroupUserRole> GroupUserRoles { get; set; }

	ICollection<TExternalGroup> ExternalGroups { get; set; }
}

public static class GroupExtensions
{
	public static bool IsEnded<TGroup, TExternalTerm, TGroupUserRole, TExternalGroup>(this TGroup group, DateTime nowUtc)
		where TGroup : IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>
		where TExternalTerm : IExternalTerm
		where TGroupUserRole : IGroupUserRole
		where TExternalGroup : IExternalGroup
	{
		return group.EndDate.HasValue && group.EndDate <= nowUtc ||
				group.ExternalTermId.HasValue && group.ExternalTerm.EndDate <= nowUtc;
	}
}