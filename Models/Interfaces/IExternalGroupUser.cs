﻿using StudioKit.Data.Entity.Identity.Interfaces;

namespace StudioKit.ExternalProvider.Models.Interfaces;

public interface IExternalGroupUser<TExternalGroup>
	where TExternalGroup : class, IExternalGroup
{
	int ExternalGroupId { get; set; }

	TExternalGroup ExternalGroup { get; set; }

	string UserId { get; set; }

	IUser User { get; set; }

	string ExternalUserId { get; set; }

	string Roles { get; set; }
}