﻿using System;

namespace StudioKit.ExternalProvider.Models.Interfaces;

public interface IExternalTerm
{
	int Id { get; set; }

	int ExternalProviderId { get; set; }

	string ExternalId { get; set; }

	string Name { get; set; }

	DateTime StartDate { get; set; }

	DateTime EndDate { get; set; }
}