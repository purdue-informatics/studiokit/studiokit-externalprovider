﻿namespace StudioKit.ExternalProvider.Models.Interfaces;

public interface IGradableJobLog
{
	int GradableId { get; set; }

	string Type { get; set; }

	string QueueMessageId { get; set; }

	/// <summary>
	/// Id referencing the "source" of this log. When automatically created by a dispatch, this will be the Id of the dispatch message.</item>
	/// </summary>
	string SourceQueueMessageId { get; set; }

	string ExceptionMessage { get; set; }
}