﻿namespace StudioKit.ExternalProvider.Models.Interfaces;

public interface IGradable
{
	int Id { get; set; }

	int GroupId { get; set; }

	bool IsDeleted { get; set; }

	string Name { get; }

	decimal? Points { get; }
}