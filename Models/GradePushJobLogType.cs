﻿namespace StudioKit.ExternalProvider.Models;

public static class GradePushJobLogType
{
	public const string Pushing = "Pushing";
	public const string PushingFailed = "PushingFailed";
	public const string Pushed = "Pushed";

	public static string[] GradePushLogTypes => new[]
	{
		Pushing,
		PushingFailed,
		Pushed
	};
}