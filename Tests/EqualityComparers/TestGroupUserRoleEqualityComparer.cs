﻿using StudioKit.Data.Entity.Extensions;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Tests.EqualityComparers;

public class TestGroupUserRoleEqualityComparer : IEqualityComparer<TestGroupUserRole>
{
	public bool Equals(TestGroupUserRole x, TestGroupUserRole y)
	{
		if (ReferenceEquals(x, y)) return true;
		if (x is null) return false;
		if (y is null) return false;
		if (!x.GetType().EntityTypeEquals(y.GetType())) return false;
		return string.Equals(x.UserId, y.UserId) && string.Equals(x.RoleId, y.RoleId) && x.GroupId == y.GroupId &&
				x.IsExternal == y.IsExternal;
	}

	public int GetHashCode(TestGroupUserRole obj)
	{
		unchecked
		{
			var hashCode = (obj.UserId != null ? obj.UserId.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ (obj.RoleId != null ? obj.RoleId.GetHashCode() : 0);
			hashCode = (hashCode * 397) ^ obj.GroupId;
			hashCode = (hashCode * 397) ^ obj.IsExternal.GetHashCode();
			return hashCode;
		}
	}
}