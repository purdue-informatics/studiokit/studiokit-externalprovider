﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Services;
using StudioKit.ExternalProvider.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.Tests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Tests.BusinessLogic.Services;

[TestClass]
public class TermSyncServiceTests : BaseTest
{
	private ILogger<TermSyncService<TestExternalTerm>> _logger;
	private IDictionary<Type, Func<ITermProviderService<TestExternalTerm>>> _termProviderServiceMapping;

	[TestInitialize]
	public void Initialize()
	{
		_logger = new Mock<ILogger<TermSyncService<TestExternalTerm>>>().Object;
		_termProviderServiceMapping = new Dictionary<Type, Func<ITermProviderService<TestExternalTerm>>>();
	}

	#region SyncTermsAsync

	[TestMethod]
	public async Task SyncTermsAsync_ShouldReturnIfNoTermSyncEnabledExternalProviders()
	{
		await using var dbContext = DbContextFactory<TestTermSyncDbContext>.CreateTransient();
		var service = GetService(dbContext);
		await service.SyncTermsAsync();
		var externalTermCount = await dbContext.ExternalTerms.CountAsync();
		Assert.AreEqual(0, externalTermCount);
	}

	[TestMethod]
	public async Task SyncTermsAsync_ShouldAddNewExternalTermsAndPreserveExisting()
	{
		await using var dbContext = DbContextFactory<TestTermSyncDbContext>.CreateTransient();
		var externalProvider = new TestExternalProvider
		{
			Name = "Test",
			TermSyncEnabled = true,
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow
		};
		dbContext.ExternalProviders.Add(externalProvider);
		await dbContext.SaveChangesAsync();

		var existingExternalTerm = new TestExternalTerm
		{
			ExternalProviderId = externalProvider.Id,
			ExternalId = "abc",
			Name = "Existing Term",
			StartDate = DateTime.UtcNow.AddDays(-30),
			EndDate = DateTime.UtcNow.AddDays(30),
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow
		};
		dbContext.ExternalTerms.Add(existingExternalTerm);
		await dbContext.SaveChangesAsync();

		var providedExternalTerms = new List<TestExternalTerm>
		{
			new()
			{
				ExternalProviderId = externalProvider.Id,
				ExternalId = "abc",
				Name = "Existing Term",
				StartDate = DateTime.UtcNow.AddDays(-30),
				EndDate = DateTime.UtcNow.AddDays(30),
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow
			},
			new()
			{
				ExternalProviderId = externalProvider.Id,
				ExternalId = "efg",
				Name = "New Term 1",
				StartDate = DateTime.UtcNow.AddDays(30),
				EndDate = DateTime.UtcNow.AddDays(60),
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow
			},
			new()
			{
				ExternalProviderId = externalProvider.Id,
				ExternalId = "xyz",
				Name = "New Term 2",
				StartDate = DateTime.UtcNow.AddDays(60),
				EndDate = DateTime.UtcNow.AddDays(90),
				DateStored = DateTime.UtcNow,
				DateLastUpdated = DateTime.UtcNow
			}
		};

		var termProviderServiceMapping =
			new Dictionary<Type, Func<ITermProviderService<TestExternalTerm>>>
			{
				{
					typeof(TestExternalProvider), () =>
					{
						var mockTermProviderService = new Mock<ITermProviderService<TestExternalTerm>>();
						mockTermProviderService
							.Setup(x => x.GetTermsAsync(It.IsAny<Models.ExternalProvider>(), It.IsAny<CancellationToken>()))
							.ReturnsAsync(providedExternalTerms);
						return mockTermProviderService.Object;
					}
				}
			};

		var service = GetService(dbContext, termProviderServiceMapping);
		await service.SyncTermsAsync();

		var externalTerms = await dbContext.ExternalTerms.ToListAsync();

		Assert.AreEqual(3, externalTerms.Count);
	}

	[TestMethod]
	public async Task SyncTermsAsync_ShouldUpdateSecondsOnEndDateTimeWhenNeeded()
	{
		await using var dbContext = DbContextFactory<TestTermSyncDbContext>.CreateTransient();

		var externalProvider = new TestExternalProvider
		{
			Name = "Test",
			TermSyncEnabled = true,
			DateStored = DateTime.UtcNow,
			DateLastUpdated = DateTime.UtcNow
		};
		dbContext.ExternalProviders.Add(externalProvider);
		await dbContext.SaveChangesAsync();

		var startTime = new DateTime(2021, 1, 1);
		var endTime1 = new DateTime(2021, 1, 1, 23, 59, 59);
		var endTime2 = new DateTime(2021, 1, 2);
		var endTime3 = new DateTime(2021, 1, 2, 0, 0, 45);
		var providedExternalTerms = new List<TestExternalTerm>
		{
			new()
			{
				ExternalProviderId = externalProvider.Id,
				ExternalId = "abc",
				Name = "New Term with correct seconds value",
				StartDate = startTime,
				EndDate = endTime1,
				DateStored = startTime,
				DateLastUpdated = startTime
			},
			new()
			{
				ExternalProviderId = externalProvider.Id,
				ExternalId = "efg",
				Name = "New Term with end date on the minute",
				StartDate = startTime,
				EndDate = endTime2,
				DateStored = startTime,
				DateLastUpdated = startTime
			},
			new()
			{
				ExternalProviderId = externalProvider.Id,
				ExternalId = "xyz",
				Name = "New Term with end date with arbitrary seconds value",
				StartDate = startTime,
				EndDate = endTime3,
				DateStored = startTime,
				DateLastUpdated = startTime
			}
		};

		var termProviderServiceMapping =
			new Dictionary<Type, Func<ITermProviderService<TestExternalTerm>>>
			{
				{
					typeof(TestExternalProvider), () =>
					{
						var mockTermProviderService = new Mock<ITermProviderService<TestExternalTerm>>();
						mockTermProviderService
							.Setup(x => x.GetTermsAsync(It.IsAny<Models.ExternalProvider>(), It.IsAny<CancellationToken>()))
							.ReturnsAsync(providedExternalTerms);
						return mockTermProviderService.Object;
					}
				}
			};

		var service = GetService(dbContext, termProviderServiceMapping);
		await service.SyncTermsAsync();

		var externalTerms = await dbContext.ExternalTerms.ToListAsync();

		// The terms should have their end date adjusted to be at 59 seconds within the minute, making all 3
		// match the first end time which already had the correct seconds value.
		Assert.AreEqual(3, externalTerms.Count(et => et.EndDate.Equals(endTime1)));
	}

	#endregion SyncTermsAsync

	#region Helpers

	private ITermSyncService GetService(
		TestTermSyncDbContext dbContext,
		IDictionary<Type, Func<ITermProviderService<TestExternalTerm>>> termProviderServiceMapping = null)
	{
		dbContext.ChangeTracker.Clear();
		return new TermSyncService<TestExternalTerm>(_logger, dbContext, termProviderServiceMapping ?? _termProviderServiceMapping);
	}

	#endregion Helpers
}