﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.BusinessLogic.Services;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.Properties;
using StudioKit.ExternalProvider.Tests.EqualityComparers;
using StudioKit.ExternalProvider.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using StudioKit.ExternalProvider.Tests.TestData.Services;
using StudioKit.Tests;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Tests.BusinessLogic.Services;

[TestClass]
public class RosterSyncServiceTests : BaseTest
{
	private static DbConnection _dbConnection;
	private TestRosterSyncDbContext _dbContext;
	private IDbContextTransaction _transaction;

	private ILogger<RosterSyncService<TestUser, TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser>>
		_logger;

	private IErrorHandler _errorHandler;
	private IDateTimeProvider _dateTimeProvider;

	/// <summary>
	/// The "current" date and time
	/// </summary>
	private readonly DateTime _dateTime = new(2020, 1, 10);

	/// <summary>
	/// The date when existing data was stored, 3 days in the past
	/// </summary>
	private readonly DateTime _dateStored = new(2020, 1, 7);

	private Role _superAdminRole;
	private Role _adminRole;
	private Role _creatorRole;
	private Role _groupOwnerRole;
	private Role _groupLearnerRole;
	private TestExternalProvider _externalProvider;
	private TestUser _user1;
	private TestUser _user2;
	private TestUser _user3;
	private TestUser _user4;
	private TestUser _nobody1;
	private TestUser _nobody2;
	private TestGroup _group;
	private TestExternalGroup _externalGroup1;
	private TestExternalGroup _externalGroup2;

	[ClassInitialize]
	public static void BeforeAll(TestContext testContext)
	{
		_dbConnection = DbConnectionFactory.CreateSqliteInMemoryConnection();
		_dbConnection.Open();

		using var dbContext = DbContextFactory<TestRosterSyncDbContext>.CreateTransient(_dbConnection);

		// TODO: seed shared data
	}

	[TestInitialize]
	public void BeforeEach()
	{
		(_dbContext, _transaction) = DbContextFactory<TestRosterSyncDbContext>.CreateTransientAndTransaction(_dbConnection);

		_logger =
			new Mock<ILogger<RosterSyncService<TestUser, TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup,
				TestExternalGroupUser>>>().Object;
		_errorHandler = new Mock<IErrorHandler>().Object;
		_dateTimeProvider = new Mock<IDateTimeProvider>().Object;

		var mockDateTimeProvider = new Mock<IDateTimeProvider>();
		mockDateTimeProvider.SetupGet(d => d.UtcNow).Returns(_dateTime);
		_dateTimeProvider = mockDateTimeProvider.Object;

		// TODO: make a TestDependencies class that holds all this info

		_superAdminRole = new Role(BaseRole.SuperAdmin);
		_adminRole = new Role(BaseRole.Admin);
		_creatorRole = new Role(BaseRole.Creator);
		_groupOwnerRole = new Role(BaseRole.GroupOwner);
		_groupLearnerRole = new Role(BaseRole.GroupLearner);

		_externalProvider = new TestExternalProvider
		{
			Name = "Test",
			RosterSyncEnabled = true,
			DateStored = _dateStored,
			DateLastUpdated = _dateStored
		};

		_user1 = new TestUser
		{
			Id = "1",
			UserName = "1",
			NormalizedUserName = "1",
			Email = "1@external.org",
			NormalizedEmail = "1@EXTERNAL.ORG",
			FirstName = "FirstName 1",
			LastName = "LastName",
			EmployeeNumber = "0000000001",
			Uid = "1"
		};
		_user2 = new TestUser
		{
			Id = "2",
			UserName = "2",
			NormalizedUserName = "2",
			Email = "2@external.org",
			NormalizedEmail = "2@EXTERNAL.ORG",
			FirstName = "FirstName 2",
			LastName = "LastName",
			EmployeeNumber = "0000000002",
			Uid = "2"
		};
		_user3 = new TestUser
		{
			Id = "3",
			UserName = "3",
			NormalizedUserName = "3",
			Email = "3@external.org",
			NormalizedEmail = "3@EXTERNAL.ORG",
			FirstName = "FirstName 3",
			LastName = "LastName",
			EmployeeNumber = "0000000003",
			Uid = "3"
		};
		_user4 = new TestUser
		{
			Id = "4",
			UserName = "4",
			NormalizedUserName = "4",
			Email = "4@external.org",
			NormalizedEmail = "4@EXTERNAL.ORG",
			FirstName = "FirstName 4",
			LastName = "LastName",
			EmployeeNumber = "0000000004",
			Uid = "4"
		};
		_nobody1 = new TestUser
		{
			Id = "5",
			UserName = "nobody-1234",
			NormalizedUserName = "NOBODY-1234",
			Email = "nobody@external.org",
			NormalizedEmail = "NOBODY@EXTERNAL.ORG",
			FirstName = "FirstName 5",
			LastName = "LastName",
			EmployeeNumber = "0000000005",
			Uid = "5"
		};
		_nobody2 = new TestUser
		{
			Id = "6",
			UserName = "nobody-5678",
			NormalizedUserName = "NOBODY-5678",
			Email = "nobody@external.org",
			NormalizedEmail = "NOBODY@EXTERNAL.ORG",
			FirstName = "FirstName 6",
			LastName = "LastName",
			EmployeeNumber = "0000000006",
			Uid = "6"
		};

		_group = new TestGroup
		{
			DateStored = _dateStored,
			DateLastUpdated = _dateStored
		};

		_externalGroup1 = new TestExternalGroup
		{
			ExternalId = "1234",
			UserId = "1",
			Name = "Course 1",
			ExternalProvider = _externalProvider,
			DateStored = _dateStored,
			DateLastUpdated = _dateStored
		};

		_externalGroup2 = new TestExternalGroup
		{
			ExternalId = "5678",
			UserId = "1",
			Name = "Course 2",
			ExternalProvider = _externalProvider,
			DateStored = _dateStored,
			DateLastUpdated = _dateStored
		};
	}

	[TestCleanup]
	public void AfterEach()
	{
		_transaction.Rollback();
		_transaction.Dispose();
		_dbContext.Dispose();
	}

	[ClassCleanup]
	public static void AfterAll()
	{
		_dbConnection.Close();
		_dbConnection.Dispose();
	}

	#region SyncGroupAsync

	[TestMethod]
	public void SyncGroupAsync_ShouldThrowIfGroupNotFound()
	{
		var service = GetService(_dbContext, GetRosterProviderServiceMapping());

		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.SyncGroupAsync(1);
			}), string.Format(Strings.RosterSyncGroupFailed, 1));
	}

	[TestMethod]
	public async Task SyncGroupAsync_ShouldThrowIfGroupIsEndedWithDates()
	{
		_group.StartDate = _dateTime.AddDays(-2);
		_group.EndDate = _dateTime.AddDays(-1);
		await SeedAsync(_dbContext);

		var service = GetService(_dbContext, GetRosterProviderServiceMapping());

		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.SyncGroupAsync(_group.Id);
			}), string.Format(Strings.RosterSyncGroupFailed, _group.Id));
	}

	[TestMethod]
	public async Task SyncGroupAsync_ShouldThrowIfGroupIsEndedWithExternalTerm()
	{
		_group.ExternalTerm = new TestExternalTerm
		{
			ExternalId = "ext-term-1",
			StartDate = _dateTime.AddDays(-2),
			EndDate = _dateTime.AddDays(-1),
			DateStored = _dateStored,
			DateLastUpdated = _dateStored,
			ExternalProvider = _externalProvider
		};
		await SeedAsync(_dbContext);

		var service = GetService(_dbContext, GetRosterProviderServiceMapping());

		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.SyncGroupAsync(_group.Id);
			}), string.Format(Strings.RosterSyncGroupFailed, _group.Id));
	}

	[TestMethod]
	public async Task SyncGroupAsync_ShouldThrowIfHasExternalGroupsButNoneHaveRosterSyncEnabled()
	{
		// disable roster sync for seed data
		_externalProvider.RosterSyncEnabled = false;
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);

		var service = GetService(_dbContext, GetRosterProviderServiceMapping());

		Assert.ThrowsAsync<Exception>(
			Task.Run(async () =>
			{
				await service.SyncGroupAsync(_group.Id);
			}), string.Format(Strings.RosterSyncGroupFailed, _group.Id));
	}

	[TestMethod]
	public async Task SyncGroupAsync_ShouldAddCrossListedStudentEnrollments()
	{
		_group.StartDate = _dateTime.AddDays(-2);
		_group.EndDate = _dateTime.AddDays(7);
		_group.ExternalGroups = new List<TestExternalGroup>
		{
			_externalGroup1,
			_externalGroup2
		};
		await SeedAsync(_dbContext);

		var course1Roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
		{
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup1.Id,
					ExternalUserId = "a",
					User = _user1
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			},
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup1.Id,
					ExternalUserId = "b",
					User = _user2
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			}
		};
		var course2Roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
		{
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup2.Id,
					ExternalUserId = "a",
					User = _user1
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			}
		};

		var rosterProviderServiceMapping = GetRosterProviderServiceMapping(mockRosterProviderService =>
		{
			mockRosterProviderService
				.Setup(x => x.GetRosterAsync(
					It.IsAny<Models.ExternalProvider>(),
					It.Is<TestExternalGroup>(eg => eg.Id == _externalGroup1.Id),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(course1Roster);

			mockRosterProviderService
				.Setup(x => x.GetRosterAsync(
					It.IsAny<Models.ExternalProvider>(),
					It.Is<TestExternalGroup>(eg => eg.Id == _externalGroup2.Id),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(course2Roster);
		});

		var service = GetService(_dbContext, rosterProviderServiceMapping);
		await service.SyncGroupAsync(_group.Id);

		var groupUserRoles = await _dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(_group.Id)).ToListAsync();
		var externalGroupUsers =
			await _dbContext.ExternalGroupUsers.Where(egu => egu.ExternalGroup.GroupId.Equals(_group.Id)).ToListAsync();

		Assert.AreEqual(2, groupUserRoles.Count, "Distinct number of GroupUserRoles");
		Assert.AreEqual(3, externalGroupUsers.Count, "Total number of ExternalGroupUsers, from all cross-listed enrollments");
	}

	[TestMethod]
	public async Task SyncGroupAsync_ShouldAllowMultipleUsersWithTheSameEmail()
	{
		_group.StartDate = _dateTime.AddDays(-2);
		_group.EndDate = _dateTime.AddDays(7);
		_group.ExternalGroups = new List<TestExternalGroup>
		{
			_externalGroup1,
			_externalGroup2
		};
		await SeedAsync(_dbContext);

		var course1Roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
		{
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup1.Id,
					ExternalUserId = "a",
					User = _nobody1
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			}
		};
		var course2Roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
		{
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup2.Id,
					ExternalUserId = "b",
					User = _nobody2
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			}
		};

		var rosterProviderServiceMapping = GetRosterProviderServiceMapping(mockRosterProviderService =>
		{
			mockRosterProviderService
				.Setup(x => x.GetRosterAsync(
					It.IsAny<Models.ExternalProvider>(),
					It.Is<TestExternalGroup>(eg => eg.Id == _externalGroup1.Id),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(course1Roster);

			mockRosterProviderService
				.Setup(x => x.GetRosterAsync(
					It.IsAny<Models.ExternalProvider>(),
					It.Is<TestExternalGroup>(eg => eg.Id == _externalGroup2.Id),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(course2Roster);
		});

		var service = GetService(_dbContext, rosterProviderServiceMapping);
		await service.SyncGroupAsync(_group.Id);

		var groupUserRoles = await _dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(_group.Id)).ToListAsync();
		var externalGroupUsers =
			await _dbContext.ExternalGroupUsers.Where(egu => egu.ExternalGroup.GroupId.Equals(_group.Id)).ToListAsync();

		Assert.AreEqual(2, groupUserRoles.Count, "Distinct number of GroupUserRoles");
		Assert.AreEqual(2, externalGroupUsers.Count, "Total number of ExternalGroupUsers, from all cross-listed enrollments");
	}

	[TestMethod]
	public async Task SyncGroupAsync_ShouldAddUpdateAndRemoveEnrollments()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);

		var roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
		{
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup1.Id,
					ExternalUserId = "c",
					User = _user3
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			},
			new()
			{
				ExternalGroupUser = new TestExternalGroupUser
				{
					ExternalGroupId = _externalGroup1.Id,
					ExternalUserId = "d",
					User = _user4
				},
				GroupUserRoles = new List<string>
				{
					BaseRole.GroupLearner
				}
			}
		};

		var rosterProviderServiceMapping = GetRosterProviderServiceMapping(mockRosterProviderService =>
		{
			mockRosterProviderService
				.Setup(x => x.GetRosterAsync(
					It.IsAny<Models.ExternalProvider>(),
					It.IsAny<TestExternalGroup>(),
					It.IsAny<CancellationToken>()))
				.ReturnsAsync(roster);
		});

		var service = GetService(_dbContext, rosterProviderServiceMapping);
		await service.SyncGroupAsync(_group.Id);

		var groupUserRoles = await _dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(_group.Id)).ToListAsync();
		var externalGroupUsers =
			await _dbContext.ExternalGroupUsers.Where(egu => egu.ExternalGroup.GroupId.Equals(_group.Id)).ToListAsync();

		Assert.IsTrue(groupUserRoles.OrderBy(gur => gur.UserId).SequenceEqual(new List<TestGroupUserRole>
		{
			new()
			{
				GroupId = _group.Id,
				UserId = _user1.Id,
				RoleId = _groupOwnerRole.Id,
				IsExternal = false
			},
			new()
			{
				GroupId = _group.Id,
				UserId = _user3.Id,
				RoleId = _groupLearnerRole.Id,
				IsExternal = true
			},
			new()
			{
				GroupId = _group.Id,
				UserId = _user4.Id,
				RoleId = _groupLearnerRole.Id,
				IsExternal = true
			}
		}, new TestGroupUserRoleEqualityComparer()), "GroupUserRoles: removed user 2, updated user 3 to IsExternal, added user 4");

		Assert.IsTrue(externalGroupUsers.SequenceEqual(new List<TestExternalGroupUser>
		{
			new()
			{
				UserId = _user3.Id,
				ExternalUserId = "c",
				ExternalGroupId = _externalGroup1.Id
			},
			new()
			{
				UserId = _user4.Id,
				ExternalUserId = "d",
				ExternalGroupId = _externalGroup1.Id
			}
		}, new TestExternalGroupUserEqualityComparer()), "ExternalGroupUsers: removed user 2, added user 3, added user 4");
	}

	[TestMethod]
	public async Task SyncGroupAsync_ShouldRemovedSyncedEnrollmentsWhenExternalGroupsRemoved()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);

		_dbContext.ExternalGroups.RemoveRange(_group.ExternalGroups);
		await _dbContext.SaveChangesAsync();

		var service = GetService(_dbContext, GetRosterProviderServiceMapping());
		await service.SyncGroupAsync(_group.Id);

		var groupUserRoles = await _dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(_group.Id)).ToListAsync();
		var externalGroupUsers =
			await _dbContext.ExternalGroupUsers.Where(egu => egu.ExternalGroup.GroupId.Equals(_group.Id)).ToListAsync();

		Assert.AreEqual(2, groupUserRoles.Count, "Removed one synced user GroupUserRoles");
		Assert.AreEqual(0, externalGroupUsers.Count, "Removed all ExternalGroupUsers");
	}

	#endregion SyncGroupAsync

	#region SyncAllGroupsAsync

	[TestMethod]
	public async Task SyncAllGroupsAsync_ShouldSucceed()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);

		var originalCount = _group.GroupUserRoles.Count;

		// mock with empty roster, which will remove one currently synced user
		var service = GetService(_dbContext, GetRosterProviderServiceMapping());
		await service.SyncAllGroupsAsync();

		var updatedCount = await _dbContext.GroupUserRoles.CountAsync(gur => gur.GroupId == _group.Id);

		Assert.AreEqual(originalCount - 1, updatedCount);
	}

	[TestMethod]
	public async Task SyncAllGroupsAsync_ShouldNotRunForDeletedGroup()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);
		_group.IsDeleted = true;
		await _dbContext.SaveChangesAsync();

		var originalCount = _group.GroupUserRoles.Count;

		// mock an empty roster, which would normally remove one currently synced user
		var service = GetService(_dbContext, GetRosterProviderServiceMapping());
		await service.SyncAllGroupsAsync();

		var updatedCount = await _dbContext.GroupUserRoles.CountAsync(gur => gur.GroupId == _group.Id);

		Assert.AreEqual(originalCount, updatedCount);
	}

	[TestMethod]
	public async Task SyncAllGroupsAsync_ShouldNotRunForPastGroupWithDates()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);
		// move group to the past
		_group.StartDate = _dateTime.AddDays(-2);
		_group.EndDate = _dateTime.AddDays(-1);
		await _dbContext.SaveChangesAsync();

		var originalCount = _group.GroupUserRoles.Count;

		// mock an empty roster, which would normally remove one currently synced user
		var service = GetService(_dbContext, GetRosterProviderServiceMapping());
		await service.SyncAllGroupsAsync();

		var updatedCount = await _dbContext.GroupUserRoles.CountAsync(gur => gur.GroupId == _group.Id);

		Assert.AreEqual(originalCount, updatedCount);
	}

	[TestMethod]
	public async Task SyncAllGroupsAsync_ShouldNotRunForPastGroupWithExternalTerm()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);
		// move group to a past term
		_group.StartDate = null;
		_group.EndDate = null;
		_group.ExternalTerm = new TestExternalTerm
		{
			ExternalId = "ext-term-1",
			StartDate = _dateTime.AddDays(-2),
			EndDate = _dateTime.AddDays(-1),
			DateStored = _dateStored,
			DateLastUpdated = _dateStored,
			ExternalProvider = _externalProvider
		};
		await _dbContext.SaveChangesAsync();

		var originalCount = _group.GroupUserRoles.Count;

		// mock with empty roster, which would normally remove one currently synced user
		var service = GetService(_dbContext, GetRosterProviderServiceMapping());
		await service.SyncAllGroupsAsync();

		var updatedCount = await _dbContext.GroupUserRoles.CountAsync(gur => gur.GroupId == _group.Id);

		Assert.AreEqual(originalCount, updatedCount);
	}

	[TestMethod]
	public async Task SyncAllGroupsAsync_ShouldNotRunForGroupWithRosterSyncDisabled()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);
		_externalProvider.RosterSyncEnabled = false;
		await _dbContext.SaveChangesAsync();

		var originalCount = _group.GroupUserRoles.Count;

		// mock an empty roster, which would normally remove one currently synced user
		var service = GetService(_dbContext, GetRosterProviderServiceMapping());

		await service.SyncAllGroupsAsync();

		var updatedCount = await _dbContext.GroupUserRoles.CountAsync(gur => gur.GroupId == _group.Id);

		Assert.AreEqual(originalCount, updatedCount);
	}

	[TestMethod]
	public async Task SyncAllGroupsAsync_ShouldThrowIfAnyFailuresOccur()
	{
		await SeedCurrentGroupWithExistingUsersAsync(_dbContext);

		var rosterProviderService = GetRosterProviderServiceMapping(mockRosterProviderService =>
		{
			mockRosterProviderService
				.Setup(x => x.GetRosterAsync(
					It.IsAny<Models.ExternalProvider>(),
					It.Is<TestExternalGroup>(eg => eg.Id == _externalGroup1.Id),
					It.IsAny<CancellationToken>()))
				.Throws(new Exception());
		});

		var service = GetService(_dbContext, rosterProviderService);

		Assert.ThrowsAsync<Exception>(
			Task.Run(async () => { await service.SyncAllGroupsAsync(); }),
			string.Format(
				Strings.RosterSyncAllGroupsFailedAndFinished,
				string.Join(", ", new[] { _group.Id }),
				string.Join(", ", new int[] { })));
	}

	#endregion SyncAllGroupsAsync

	#region Helpers

	private static Dictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>> GetRosterProviderServiceMapping(
		Action<Mock<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>> setupRosterProviderService = null)
	{
		return new Dictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>>
		{
			{
				typeof(TestExternalProvider),
				() =>
				{
					var mockRosterProviderService =
						new Mock<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>();

					// allow customizing the provider, otherwise it returns a blank roster
					if (setupRosterProviderService != null)
					{
						setupRosterProviderService(mockRosterProviderService);
					}
					else
					{
						mockRosterProviderService
							.Setup(x => x.GetRosterAsync(
								It.IsAny<Models.ExternalProvider>(),
								It.IsAny<TestExternalGroup>(),
								It.IsAny<CancellationToken>()))
							.ReturnsAsync(
								new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>());
					}

					return mockRosterProviderService.Object;
				}
			}
		};
	}

	private IRosterSyncService GetService(
		TestRosterSyncDbContext dbContext,
		IDictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>> rosterProviderServiceMapping)
	{
		dbContext.ChangeTracker.Clear();
		return new RosterSyncService<TestUser, TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser>(
			_errorHandler,
			_logger,
			dbContext,
			rosterProviderServiceMapping,
			_dateTimeProvider,
			new TestGroupUserRoleService(dbContext));
	}

	private async Task SeedAsync(TestRosterSyncDbContext dbContext, CancellationToken cancellationToken = default)
	{
		dbContext.Roles.AddRange(new List<Role>
		{
			_superAdminRole,
			_adminRole,
			_creatorRole,
			_groupOwnerRole,
			_groupLearnerRole
		});

		dbContext.ExternalProviders.Add(_externalProvider);
		dbContext.Users.AddRange(new List<TestUser>
		{
			_user1,
			_user2,
			_user3,
			_user4
		});
		dbContext.Groups.Add(_group);

		await _dbContext.SaveChangesAsync(cancellationToken);
	}

	private async Task SeedCurrentGroupWithExistingUsersAsync(TestRosterSyncDbContext dbContext,
		CancellationToken cancellationToken = default)
	{
		_group.StartDate = _dateTime.AddDays(-2);
		_group.EndDate = _dateTime.AddDays(7);
		_group.ExternalGroups = new List<TestExternalGroup>
		{
			_externalGroup1
		};
		_group.GroupUserRoles = new List<TestGroupUserRole>
		{
			// existing manually added instructor, should not be touched
			new()
			{
				UserId = _user1.Id,
				RoleId = _groupOwnerRole.Id,
				IsExternal = false,
				DateStored = _dateStored,
				DateLastUpdated = _dateStored
			},
			// existing external learner, to be removed
			new()
			{
				UserId = _user2.Id,
				RoleId = _groupLearnerRole.Id,
				IsExternal = true,
				DateStored = _dateStored,
				DateLastUpdated = _dateStored
			},
			// existing manually added learner, to be converted to an external learner
			new()
			{
				UserId = _user3.Id,
				RoleId = _groupLearnerRole.Id,
				IsExternal = false,
				DateStored = _dateStored,
				DateLastUpdated = _dateStored
			}
		};
		_group.GroupUserRoleLogs = new List<TestGroupUserRoleLog>
		{
			new()
			{
				UserId = _user1.Id,
				RoleId = _groupOwnerRole.Id,
				Type = GroupUserRoleLogType.Added,
				DateStored = _dateStored,
				DateLastUpdated = _dateStored
			},
			new()
			{
				UserId = _user2.Id,
				RoleId = _groupLearnerRole.Id,
				Type = GroupUserRoleLogType.Added,
				DateStored = _dateStored,
				DateLastUpdated = _dateStored
			},
			new()
			{
				UserId = _user3.Id,
				RoleId = _groupLearnerRole.Id,
				Type = GroupUserRoleLogType.Added,
				DateStored = _dateStored,
				DateLastUpdated = _dateStored
			}
		};
		dbContext.ExternalGroupUsers.AddRange(new List<TestExternalGroupUser>
		{
			new()
			{
				UserId = _user2.Id,
				ExternalUserId = "b",
				ExternalGroup = _externalGroup1,
				DateStored = _dateStored,
				DateLastUpdated = _dateStored
			}
		});
		await SeedAsync(_dbContext, cancellationToken);
	}

	#endregion Helpers
}