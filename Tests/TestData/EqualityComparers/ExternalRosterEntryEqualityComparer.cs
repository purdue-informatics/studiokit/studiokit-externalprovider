﻿using StudioKit.Data.Entity.Extensions;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.ExternalProvider.Tests.TestData.EqualityComparers;

public class ExternalRosterEntryEqualityComparer : IEqualityComparer<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
{
	public bool Equals(ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser> x,
		ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser> y)
	{
		if (x == y) return true;
		if (x is null) return false;
		if (y is null) return false;
		if (!x.GetType().EntityTypeEquals(y.GetType())) return false;
		return new TestExternalGroupUserEqualityComparer().Equals(x.ExternalGroupUser, y.ExternalGroupUser) &&
				x.GroupUserRoles.OrderBy(r => r).SequenceEqual(y.GroupUserRoles.OrderBy(r => r));
	}

	public int GetHashCode(ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser> obj)
	{
		unchecked
		{
			var hashCode = obj.ExternalGroupUser != null ? obj.ExternalGroupUser.GetHashCode() : 0;
			hashCode = (hashCode * 397) ^ (obj.GroupUserRoles != null ? obj.GroupUserRoles.GetHashCode() : 0);
			return hashCode;
		}
	}
}