﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Tests.TestData.Services;

public class TestGroupUserRoleService : IGroupUserRoleService<TestGroupUserRole>
{
	private readonly TestRosterSyncDbContext _dbContext;

	public TestGroupUserRoleService(TestRosterSyncDbContext dbContext)
	{
		_dbContext = dbContext;
	}

	public async Task AddRangeAsync(List<TestGroupUserRole> entityUserRoles, IPrincipal principal,
		CancellationToken cancellationToken = default)
	{
		_dbContext.GroupUserRoles.AddRange(entityUserRoles);
		await _dbContext.SaveChangesAsync(cancellationToken);
	}

	public async Task DeleteRangeAsync(List<TestGroupUserRole> entityUserRoles, IPrincipal principal, bool throwIfRemovingAllOwners = true,
		CancellationToken cancellationToken = default)
	{
		_dbContext.GroupUserRoles.RemoveRange(entityUserRoles);
		await _dbContext.SaveChangesAsync(cancellationToken);
	}
}