using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Data.Common;

namespace StudioKit.ExternalProvider.Tests.TestData.DataAccess;

public class TestTermSyncDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
	ITermSyncDbContext<TestExternalTerm>
{
	public TestTermSyncDbContext()
	{
	}

	public TestTermSyncDbContext(DbContextOptions options) : base(options)
	{
	}

	public TestTermSyncDbContext(DbConnection existingConnection) : base(existingConnection)
	{
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<TestUser>(b =>
		{
			b.HasIndex(e => e.Uid);
			b.HasIndex(e => e.EmployeeNumber);
		});
	}

	public DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

	public DbSet<TestExternalProvider> TestExternalProviders { get; set; }

	public DbSet<TestExternalTerm> ExternalTerms { get; set; }
}