using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Tests.TestData.DataAccess;

public class TestRosterSyncDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
	IRosterSyncDbContext<TestUser, TestGroup, TestGroupUserRole, TestExternalTerm, TestExternalGroup, TestExternalGroupUser>
{
	public TestRosterSyncDbContext()
	{
	}

	public TestRosterSyncDbContext(DbContextOptions options) : base(options)
	{
	}

	public TestRosterSyncDbContext(DbConnection existingConnection) : base(existingConnection)
	{
	}

	protected override void OnModelCreating(ModelBuilder modelBuilder)
	{
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<TestAssignment>(b =>
		{
			b.HasIndex(e => e.IsDeleted);
		});

		modelBuilder.Entity<TestExternalGroup>(b =>
		{
			b.HasOne(l => (TestUser)l.User);

			b.HasIndex(e => new { e.GroupId, e.ExternalId, e.ExternalProviderId })
				.IsUnique();
		});

		modelBuilder.Entity<TestExternalGroupUser>(b =>
		{
			b.HasOne(l => (TestUser)l.User);

			b.HasIndex(e => new { e.ExternalGroupId, e.UserId });
		});

		modelBuilder.Entity<TestExternalTerm>(b =>
		{
			b.HasIndex(e => new { e.ExternalProviderId, e.ExternalId });
		});

		modelBuilder.Entity<TestGroup>(b =>
		{
			b.HasIndex(e => e.IsDeleted);
		});

		modelBuilder.Entity<TestGroupAssignment>(b =>
		{
			b.HasIndex(e => e.GroupId);
			b.HasIndex(e => e.AssignmentId);
			b.HasIndex(e => e.IsDeleted);
		});

		modelBuilder.Entity<TestGroupUserRole>(b =>
		{
			b.HasIndex(e => new { e.UserId, e.RoleId, e.GroupId })
				.IsUnique();
		});

		modelBuilder.Entity<TestUser>(b =>
		{
			b.HasIndex(e => e.Uid);
			b.HasIndex(e => e.EmployeeNumber);
		});
	}

	public DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

	public DbSet<TestExternalProvider> TestExternalProviders { get; set; }

	public DbSet<TestExternalTerm> ExternalTerms { get; set; }

	public DbSet<TestExternalGroup> ExternalGroups { get; set; }

	public DbSet<TestExternalGroupUser> ExternalGroupUsers { get; set; }

	public DbSet<TestGroup> Groups { get; set; }

	public DbSet<TestGroupUserRole> GroupUserRoles { get; set; }

	public async Task<IEnumerable<TestUser>> GetOrCreateUsersAsync(IEnumerable<TestUser> usersEnumerable,
		CancellationToken cancellationToken = default)
	{
		var users = usersEnumerable.ToList();
		var usernames = users.Select(u => u.NormalizedUserName).ToList();
		// Note: not updating user details
		var existingUsers = await Users.Where(u => usernames.Contains(u.NormalizedUserName)).ToListAsync(cancellationToken);
		var usersToAdd = users.Where(ul => !existingUsers.Any(e => e.NormalizedUserName.Equals(ul.NormalizedUserName))).ToList();
		Users.AddRange(usersToAdd);
		await SaveChangesAsync(cancellationToken);
		return new List<TestUser>(existingUsers.Concat(usersToAdd));
	}
}