using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestGroup : ModelBase, IGroup<TestExternalTerm, TestGroupUserRole, TestExternalGroup>
{
	public bool IsDeleted { get; set; }

	public DateTime? StartDate { get; set; }

	public DateTime? EndDate { get; set; }

	public int? ExternalTermId { get; set; }

	[ForeignKey(nameof(ExternalTermId))]
	public TestExternalTerm ExternalTerm { get; set; }

	public ICollection<TestGroupUserRole> GroupUserRoles { get; set; }

	public ICollection<TestGroupUserRoleLog> GroupUserRoleLogs { get; set; }

	public ICollection<TestExternalGroup> ExternalGroups { get; set; }
}