using StudioKit.Data;
using StudioKit.Data.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestAssignment : ModelBase, IDelible, IAssignment
{
	[Required]
	public string Name { get; set; }

	public decimal? Points { get; set; }

	public bool IsDeleted { get; set; }

	public ICollection<TestGroupAssignment> GroupAssignments { get; set; }
}