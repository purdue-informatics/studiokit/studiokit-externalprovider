using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestGroupAssignmentJobLog : ModelBase, IGradableJobLog
{
	[Required]
	public int GradableId { get; set; }

	[ForeignKey(nameof(GradableId))]
	public TestGroupAssignment GroupAssignment { get; set; }

	[Required]
	public string Type { get; set; }

	public string QueueMessageId { get; set; }

	public string SourceQueueMessageId { get; set; }

	public string ExceptionMessage { get; set; }
}