using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestScore : ModelBase, IScore
{
	public string CreatedById { get; set; }

	public decimal? TotalPoints { get; set; }

	/// <summary>
	/// For easy querying in tests
	/// </summary>
	[Required]
	public int GroupAssignmentId { get; set; }

	[ForeignKey(nameof(GroupAssignmentId))]
	public TestGroupAssignment GroupAssignment { get; set; }

	public string UserId { get; set; }
}