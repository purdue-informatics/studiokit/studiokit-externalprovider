using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestExternalGroupAssignment : ModelBase, IExternalGradable
{
	[Required]
	public int ExternalGroupId { get; set; }

	[ForeignKey(nameof(ExternalGroupId))]
	public TestExternalGroup ExternalGroup { get; set; }

	[Required]
	public int GroupAssignmentId { get; set; }

	[NotMapped]
	public int GradableId
	{
		get => GroupAssignmentId;
		set => GroupAssignmentId = value;
	}

	[ForeignKey(nameof(GroupAssignmentId))]
	public TestGroupAssignment GroupAssignment { get; set; }

	public string ExternalId { get; set; }
}