using StudioKit.Data;
using StudioKit.Data.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestGroupAssignment : ModelBase, IDelible, IGradable
{
	[Required]
	public int GroupId { get; set; }

	[ForeignKey(nameof(GroupId))]
	public TestGroup Group { get; set; }

	[Required]
	public int AssignmentId { get; set; }

	[ForeignKey(nameof(AssignmentId))]
	public TestAssignment Assignment { get; set; }

	public bool IsDeleted { get; set; }

	public string Name => Assignment.Name;

	public decimal? Points => Assignment.Points;

	public ICollection<TestGroupAssignmentJobLog> Logs { get; set; }
}