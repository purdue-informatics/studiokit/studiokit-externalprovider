using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestExternalGroupUser : ModelBase, IExternalGroupUser<TestExternalGroup>
{
	[Required]
	public int ExternalGroupId { get; set; }

	[ForeignKey(nameof(ExternalGroupId))]
	public TestExternalGroup ExternalGroup { get; set; }

	[Required]
	public string UserId { get; set; }

	[ForeignKey(nameof(UserId))]
	public IUser User { get; set; }

	[Required]
	public string ExternalUserId { get; set; }

	public string Roles { get; set; }
}