using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models;

public class TestGroupUserRole : ModelBase, IGroupUserRole
{
	[Required]
	public string UserId { get; set; }

	[Required]
	public string RoleId { get; set; }

	[ForeignKey(nameof(UserId))]
	public TestUser User { get; set; }

	[ForeignKey(nameof(RoleId))]
	public Role Role { get; set; }

	public int GroupId { get; set; }

	public int EntityId { get => GroupId; set => GroupId = value; }

	[ForeignKey(nameof(GroupId))]
	public TestGroup Group { get; set; }

	public bool IsExternal { get; set; }
}