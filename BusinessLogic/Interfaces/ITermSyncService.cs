﻿using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.BusinessLogic.Interfaces;

public interface ITermSyncService
{
	Task SyncTermsAsync(CancellationToken cancellationToken = default);
}