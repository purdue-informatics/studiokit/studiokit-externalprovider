﻿using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.BusinessLogic.Interfaces;

public interface ITermProviderService<TExternalTerm>
	where TExternalTerm : class, IExternalTerm
{
	Task<IEnumerable<TExternalTerm>> GetTermsAsync(ExternalProvider.Models.ExternalProvider externalProvider,
		CancellationToken cancellationToken = default);
}