﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudioKit.Data.Entity.Extensions;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.Properties;
using StudioKit.Security.BusinessLogic.Models;
using StudioKit.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.BusinessLogic.Services;

public class RosterSyncService<TUser, TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> : IRosterSyncService
	where TUser : class, IUser, new()
	where TGroup : class, IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>, new()
	where TGroupUserRole : class, IGroupUserRole, new()
	where TExternalTerm : class, IExternalTerm
	where TExternalGroup : class, IExternalGroup, new()
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
{
	private readonly IErrorHandler _errorHandler;
	private readonly ILogger<RosterSyncService<TUser, TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser>> _logger;
	private readonly IRosterSyncDbContext<TUser, TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> _dbContext;
	private readonly IDictionary<Type, Func<IRosterProviderService<TExternalGroup, TExternalGroupUser>>> _rosterProviderServiceMapping;
	private readonly IDateTimeProvider _dateTimeProvider;
	private readonly IGroupUserRoleService<TGroupUserRole> _groupUserRoleService;

	public RosterSyncService(
		IErrorHandler errorHandler,
		ILogger<RosterSyncService<TUser, TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser>> logger,
		IRosterSyncDbContext<TUser, TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser> dbContext,
		IDictionary<Type, Func<IRosterProviderService<TExternalGroup, TExternalGroupUser>>> rosterProviderServiceMapping,
		IDateTimeProvider dateTimeProvider,
		IGroupUserRoleService<TGroupUserRole> groupUserRoleService)
	{
		_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
		_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_rosterProviderServiceMapping =
			rosterProviderServiceMapping ?? throw new ArgumentNullException(nameof(rosterProviderServiceMapping));
		_dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
		_groupUserRoleService = groupUserRoleService ?? throw new ArgumentNullException(nameof(groupUserRoleService));
	}

	public async Task SyncGroupAsync(int groupId, CancellationToken cancellationToken = default)
	{
		try
		{
			var principal = new SystemPrincipal();

			var group = await _dbContext.Groups
				.Include(g => g.ExternalTerm)
				.SingleOrDefaultAsync(g => g.Id == groupId && !g.IsDeleted, cancellationToken);
			if (group == null)
				throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, typeof(TGroup).Name, groupId));

			if (group.IsEnded<TGroup, TExternalTerm, TGroupUserRole, TExternalGroup>(_dateTimeProvider.UtcNow))
			{
				throw new Exception(string.Format(Strings.RosterSyncGroupPastEndDate, groupId));
			}

			var externalGroups = await _dbContext.ExternalGroups.Where(gur => gur.GroupId.Equals(groupId))
				.Include(eg => eg.ExternalProvider)
				.ToListAsync(cancellationToken);
			var existingGroupUserRoles = await _dbContext.GroupUserRoles
				.Where(gur => gur.GroupId.Equals(groupId))
				.ToListAsync(cancellationToken);
			var existingExternalGroupUsers = await _dbContext.ExternalGroupUsers
				.Where(egu => egu.ExternalGroup.GroupId.Equals(groupId))
				.ToListAsync(cancellationToken);

			// load all roles, to map names to ids
			var roles = await _dbContext.Roles.ToListAsync(cancellationToken);

			// roster un-synced
			// removed all ExternalGroups, delete existing ExternalUserRoles and external GroupUserRoles, if any
			if (!externalGroups.Any())
			{
				var groupUserRolesToDelete = existingGroupUserRoles.Where(gur => gur.IsExternal).ToList();
				_dbContext.ExternalGroupUsers.RemoveRange(existingExternalGroupUsers);
				await _groupUserRoleService.DeleteRangeAsync(groupUserRolesToDelete, principal, throwIfRemovingAllOwners: false,
					cancellationToken);
				return;
			}

			if (!externalGroups.Any(eg => eg.ExternalProvider.RosterSyncEnabled))
			{
				throw new Exception(string.Format(Strings.RosterSyncGroupNotEnabled, groupId));
			}

			// get rosters from all ExternalGroups
			var fullRoster = new List<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>();
			foreach (var externalGroup in externalGroups)
			{
				var externalProvider = externalGroup.ExternalProvider;
				var rosterProviderService = _rosterProviderServiceMapping[externalProvider.GetType().GetEntityType()]();
				var roster = await rosterProviderService.GetRosterAsync(externalProvider, externalGroup, cancellationToken);
				fullRoster.AddRange(roster);
			}

			// filter distinct users by UserName
			var fullRosterUsers = fullRoster
				.Select(r => r.ExternalGroupUser.User)
				.GroupBy(u => u.NormalizedUserName)
				.SelectMany(g => g.Take(1))
				.Cast<TUser>()
				.ToList();
			// create users in db, or get existing
			var savedUsers = await _dbContext.GetOrCreateUsersAsync(fullRosterUsers, cancellationToken);

			//
			// Construct New Data from Roster
			//

			var newGroupUserRoles = fullRoster
				.Where(externalRosterEntry => externalRosterEntry.GroupUserRoles != null)
				// group by user
				.GroupBy(externalRosterEntry => externalRosterEntry.ExternalGroupUser.User.NormalizedUserName)
				.SelectMany(userGroup =>
					// find user's distinct roles for the group to avoid duplicates
					userGroup
						.SelectMany(externalRosterEntry => externalRosterEntry.GroupUserRoles)
						.Distinct()
						.Select(n => new TGroupUserRole
						{
							GroupId = groupId,
							UserId = savedUsers.Single(u => u.NormalizedUserName.Equals(userGroup.Key)).Id,
							RoleId = roles.Single(r => r.Name.Equals(n)).Id,
							IsExternal = true
						}))
				.ToList();

			var newExternalGroupUsers = fullRoster
				.Select(externalRosterEntry =>
				{
					// reference loaded User
					var userId = savedUsers.Single(u =>
						u.NormalizedUserName.Equals(externalRosterEntry.ExternalGroupUser.User.NormalizedUserName)).Id;
					var externalGroupUser = externalRosterEntry.ExternalGroupUser;
					externalGroupUser.UserId = userId;
					externalGroupUser.User = null;
					return externalGroupUser;
				})
				.ToList();

			//
			// Add New
			//

			// add new ExternalGroupUsers
			var externalGroupUsersToAdd = newExternalGroupUsers
				.Where(egu => !existingExternalGroupUsers.Any(eegu =>
					eegu.UserId == egu.UserId &&
					eegu.ExternalGroupId == egu.ExternalGroupId))
				.ToList();
			_dbContext.ExternalGroupUsers.AddRange(externalGroupUsersToAdd);

			// add new GroupUserRoles
			var groupUserRolesToAdd = newGroupUserRoles
				.Where(gur => !existingGroupUserRoles.Any(egur =>
					egur.UserId == gur.UserId &&
					egur.RoleId == gur.RoleId))
				.ToList();

			await _groupUserRoleService.AddRangeAsync(groupUserRolesToAdd, principal, cancellationToken);

			//
			// Update Existing
			//

			// update existing local GroupUserRoles to be tracked as external if in new roster
			var groupUserRolesToUpdate = existingGroupUserRoles
				.Where(gur =>
					!gur.IsExternal &&
					newGroupUserRoles.Any(egur =>
						egur.UserId == gur.UserId &&
						egur.RoleId == gur.RoleId))
				.ToList();
			foreach (var groupUserRole in groupUserRolesToUpdate)
			{
				groupUserRole.IsExternal = true;
			}

			await _dbContext.SaveChangesAsync(cancellationToken);

			//
			// Delete Existing
			//

			// remove existing ExternalGroupUsers if not in new roster
			var externalGroupUsersToRemove = existingExternalGroupUsers
				.Where(egu =>
					!newExternalGroupUsers.Any(eegu =>
						eegu.UserId == egu.UserId &&
						eegu.ExternalGroupId == egu.ExternalGroupId))
				.ToList();
			_dbContext.ExternalGroupUsers.RemoveRange(externalGroupUsersToRemove);

			// remove existing external GroupUserRoles if not in new roster
			var groupUserRolesToRemove = existingGroupUserRoles
				.Where(gur =>
					gur.IsExternal &&
					!newGroupUserRoles.Any(egur =>
						egur.UserId == gur.UserId &&
						egur.RoleId == gur.RoleId))
				.ToList();

			await _groupUserRoleService.DeleteRangeAsync(groupUserRolesToRemove, principal, throwIfRemovingAllOwners: false,
				cancellationToken);
		}
		catch (Exception e)
		{
			var message = string.Format(Strings.RosterSyncGroupFailed, groupId);
			_logger.LogError(message);
			throw new Exception(message, e);
		}
	}

	public async Task SyncAllGroupsAsync(CancellationToken cancellationToken = default)
	{
		var now = _dateTimeProvider.UtcNow;
		var groupIds = await _dbContext.Groups
			.Where(g => !g.IsDeleted &&
						(g.EndDate.HasValue && g.EndDate > now ||
						g.ExternalTermId.HasValue && g.ExternalTerm.EndDate > now) &&
						(g.ExternalGroups.Any(eg => eg.ExternalProvider.RosterSyncEnabled) ||
						!g.ExternalGroups.Any() && g.GroupUserRoles.Any(gur => gur.IsExternal)))
			.Select(g => g.Id)
			.ToListAsync(cancellationToken);
		var successGroupIds = new List<int>();
		var failedGroupIds = new List<int>();
		foreach (var groupId in groupIds)
		{
			try
			{
				await SyncGroupAsync(groupId, cancellationToken);
				successGroupIds.Add(groupId);
			}
			catch (Exception e)
			{
				_errorHandler.CaptureException(e);
				failedGroupIds.Add(groupId);
			}
		}

		if (failedGroupIds.Any())
		{
			var message = string.Format(
				Strings.RosterSyncAllGroupsFailedAndFinished,
				string.Join(", ", failedGroupIds),
				string.Join(", ", successGroupIds));
			_logger.LogError(message);
			throw new Exception(message);
		}
	}
}