﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using StudioKit.Data.Entity.Extensions;

namespace StudioKit.ExternalProvider.BusinessLogic.Services;

public class TermSyncService<TExternalTerm> : ITermSyncService
	where TExternalTerm : class, IExternalTerm, new()
{
	private readonly ILogger<TermSyncService<TExternalTerm>> _logger;
	private readonly ITermSyncDbContext<TExternalTerm> _dbContext;
	private readonly IDictionary<Type, Func<ITermProviderService<TExternalTerm>>> _termProviderServiceMapping;

	public TermSyncService(
		ILogger<TermSyncService<TExternalTerm>> logger,
		ITermSyncDbContext<TExternalTerm> dbContext,
		IDictionary<Type, Func<ITermProviderService<TExternalTerm>>> termProviderServiceMapping)
	{
		_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
		_termProviderServiceMapping = termProviderServiceMapping ??
									throw new ArgumentNullException(nameof(termProviderServiceMapping));
	}

	public async Task SyncTermsAsync(CancellationToken cancellationToken = default)
	{
		var externalProviders = await _dbContext.ExternalProviders.Where(p => p.TermSyncEnabled)
			.ToListAsync(cancellationToken);
		if (!externalProviders.Any())
			return;

		var existingExternalTerms = await _dbContext.ExternalTerms.ToListAsync(cancellationToken);

		var allExternalTerms = new List<TExternalTerm>();
		foreach (var externalProvider in externalProviders)
		{
			var externalProviderKey = externalProvider.GetType().GetEntityType();
			var termProviderService = _termProviderServiceMapping[externalProviderKey]();
			var externalTerms = await termProviderService.GetTermsAsync(externalProvider, cancellationToken);
			allExternalTerms.AddRange(externalTerms);
		}

		_logger.LogDebug($"Loaded {allExternalTerms.Count} ExternalTerms");

		var externalTermsToAdd = allExternalTerms
			.Where(et => !existingExternalTerms.Any(eet =>
				et.ExternalProviderId == eet.ExternalProviderId &&
				et.ExternalId == eet.ExternalId))
			.ToList();

		// Ensure the term end date is at 59 seconds on the minute for consistency with our manually created dates.
		// Round to an earlier minute if not.
		foreach (var externalTerm in externalTermsToAdd)
		{
			var endDate = externalTerm.EndDate;
			if (endDate.Second != 59)
			{
				externalTerm.EndDate = endDate.AddSeconds(-1 * endDate.Second - 1);
			}
		}

		_logger.LogDebug($"Adding {externalTermsToAdd.Count} ExternalTerms");

		_dbContext.ExternalTerms.AddRange(externalTermsToAdd);

		await _dbContext.SaveChangesAsync(cancellationToken);
	}
}