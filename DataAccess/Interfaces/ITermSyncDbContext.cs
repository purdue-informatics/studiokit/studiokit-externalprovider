﻿using Microsoft.EntityFrameworkCore;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.DataAccess.Interfaces;

public interface ITermSyncDbContext<TExternalTerm>
	where TExternalTerm : class, IExternalTerm, new()
{
	Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

	#region DbSets

	DbSet<Models.ExternalProvider> ExternalProviders { get; set; }

	DbSet<TExternalTerm> ExternalTerms { get; set; }

	#endregion DbSets
}