﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.DataAccess.Interfaces;

public interface IRosterSyncDbContext<TUser, TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser>
	where TGroup : class, IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>, new()
	where TGroupUserRole : class, IGroupUserRole, new()
	where TExternalTerm : class, IExternalTerm
	where TExternalGroup : class, IExternalGroup, new()
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
{
	Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

	Task<IEnumerable<TUser>> GetOrCreateUsersAsync(IEnumerable<TUser> users, CancellationToken cancellationToken = default);

	#region DbSets

	DbSet<Role> Roles { get; set; }

	DbSet<UserRole> IdentityUserRoles { get; set; }

	DbSet<TExternalTerm> ExternalTerms { get; set; }

	DbSet<TGroup> Groups { get; set; }

	DbSet<TGroupUserRole> GroupUserRoles { get; set; }

	DbSet<Models.ExternalProvider> ExternalProviders { get; set; }

	DbSet<TExternalGroup> ExternalGroups { get; set; }

	DbSet<TExternalGroupUser> ExternalGroupUsers { get; set; }

	#endregion DbSets
}