﻿using Microsoft.EntityFrameworkCore;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.DataAccess.Interfaces;

public interface IGradePushDbContext<TGroup, TGroupUserRole, TExternalTerm, TExternalGroup, TExternalGroupUser, TExternalGradable,
	TGradable, TScore, TGradableJobLog>
	where TGroup : class, IGroup<TExternalTerm, TGroupUserRole, TExternalGroup>
	where TGroupUserRole : class, IGroupUserRole
	where TExternalTerm : class, IExternalTerm
	where TExternalGroup : class, IExternalGroup
	where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>
	where TExternalGradable : class, IExternalGradable, new()
	where TGradable : class, IGradable
	where TScore : class, IScore
	where TGradableJobLog : class, IGradableJobLog, new()
{
	Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

	#region DbSets

	DbSet<Models.ExternalProvider> ExternalProviders { get; set; }

	DbSet<TExternalTerm> ExternalTerms { get; set; }

	DbSet<TGroup> Groups { get; set; }

	DbSet<TExternalGroup> ExternalGroups { get; set; }

	DbSet<TExternalGroupUser> ExternalGroupUsers { get; set; }

	#endregion DbSets

	#region Methods

	Task<List<TScore>> GetScoresAsync(int gradableId, CancellationToken cancellationToken);

	/// <summary>
	/// Returns gradable after checking for null/deleted gradable, assignment/assessment, and group
	/// </summary>
	Task<IGradable> GetGradableAsync(int gradableId, CancellationToken cancellationToken);

	DbSet<TGradable> GetGradables();

	DbSet<TGradableJobLog> GetGradableJobLogs();

	DbSet<TExternalGradable> GetExternalGradables();

	Task<List<TExternalGradable>> GetExternalGradablesAsync(int gradableId, CancellationToken cancellationToken = default);

	#endregion Methods
}